from random import randint
import sys


def generate_array():
    return sorted([
        randint(1, 1000) for i in range(100)
    ])


def binary_search(array, target):
    left, right = 0, len(array) - 1
    while left <= right:
        mid = (left + right) // 2
        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Ошибка: неверное количество аргументов в команде")
        sys.exit(1)

    target = int(sys.argv[1])
    array = generate_array()
    index = binary_search(array, target)

    if index != -1:
        print(f'Массив: {array}')
        print(f'Введённое значение {target} найдено в массиве под индексом {index}')
    else:
        print(f'Введённое значение {target} не найдено в массиве')
