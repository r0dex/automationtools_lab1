#FROM python:3.12-slim
#COPY 2.py .
#ENTRYPOINT ["python", "2.py"]

FROM python:3.12-slim
WORKDIR /app
RUN pip install redis fastapi uvicorn
COPY . .
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080"]