from fastapi import FastAPI
import redis
import random
import uvicorn

app = FastAPI()
client = redis.Redis(host='redis', port=6379, db=0)


def get_array():
    if client.exists('array'):
        arr = client.get('array')
        array_str = arr.decode()
        array_str = array_str[1:-1]
        array = [int(i) for i in array_str.split(',')]
    else:
        array = sorted([random.randint(1, 1000) for _ in range(100)])
        arr = ','.join(map(str, array))
        client.set('array', arr)
    return array


def binary_search(array, target):
    left, right = 0, len(array) - 1
    while left <= right:
        mid = (left + right) // 2
        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1


@app.get("/{target}")
async def read_root(target: int):
    array = get_array()
    index = binary_search(array, target)
    return f'Индекс: {index}'


if __name__ == '__main__':
    uvicorn.run(app, host='localhost', port=8080)
